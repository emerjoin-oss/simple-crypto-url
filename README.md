# Crypto-URL
A small Java library to generate Crypto-URLs using JSON Web tokens.

## Maven Coordinates
Maven artifact coordinates:
```xml
<dependency>
    <groupId>org.emerjoin</groupId>
    <artifactId>simple-crypto-url</artifactId>
    <version>2.0.0-SNAPSHOT</version>
</dependency>
```
Maven repository coordinates:
```xml
<repository>
    <id>emerjoin-oss</id>
    <name>snapshot</name>
    <url>https://pkg.emerjoin.org/artifactory/oss-snapshot</url>
    <snapshots>
        <enabled>true</enabled>
    </snapshots>
</repository>
```

## Concepts
This library was designed around 3 core concepts:
* Crypto-URL
* Signal
* Propeller

### Crypto-URL
An encrypted URL that emits a Signal when accessed. 
It contains the details of the signal to be emitted when accessing it and it might have a limited validity (time).
All the data on the Crypto-URL is protected by encryption.

### Signal
Represents an application event/occurrence.

### Propeller
Component responsible for propelling signals. A propeller is expected to deliver a signal
to any application component interested in the signal. The signal subscription approach is a concern of the propeller.


#### Default Propeller
The library comes with a single **propeller** implementation: **DefaultPropeller**. This implementation propels signals
using **CDI Events**. Meaning that all you have to do is to use the **@Observes** annotation.

## User Manual
To get going, you need to take four steps:
* Create Signal POJOs
* Configure the library
* Subscribe to Signals
* Generate Crypto-URLs to emit Signals

### Create signal POJOs
This step is all about identifying the events that you want to trigger using Crypto-Urls and create a POJO for each.
Let us show you an example:
```java

public class PasswordResetConfirmation extends Signal {

    private String email;
        
    public void setEmail(String email){
        this.email = email;
    }
    
    public String getEmail(){
        return this.email;
    }

}

```

### Configure the Library
Essentially, you need to Bootstrap the library. Here is how you do it:
```java
String baseUrl = "http://myhostname/somewhere/"; //Make sure it ends with a slash
String secret = //Put some key here to be used to encrypt the JWT
CryptoUrlSettings settings = new CryptoUrlSettings(baseUrl,secret);

//Create context
CryptoUrlContext.builder()
    .settings(settings)
    .propeller(new DefaultPropeller())
    .register(PasswordResetConfirmed.class)
    .register(AnotherSignalPojo.class)
    .register(AnotherCoolSignalPojo.class)
    .build()
    .set();
```

You also need to add the **CryptoURLResource** to your **JAX-RS** application. See the example below:
```java
    @ApplicationPath("/my")
    public class MyRestApp extends Application {

        public Set<Class<?>> getClasses() {
            Set<Class<?>> set = new HashSet<>();          
            set.add(CryptoURLResource.class);
            //add more resources here
            return set;
        }

    }
```
The **CryptoURLResource** will be available at **/my/callback**.

#### Subscribe to the signals
Assuming that you decided to use the **DefaultPropeller**, then you just use CDI Events API.
Let's subscribe to the **PasswordResetConfirmation** signal:
```java
 public class MyComponent {
        
    public void performPasswordReset(@Observes PasswordResetConfirmation signal){
        //TODO: React to the signal
    }

 }

```

#### Generate Crypto-URLs
It is as simple as:
```java

//Create signal Pojo instance
PasswordResetConfirmed signal = new PasswordResetConfirmed(); 
signal.setEmail("john.doe@gmail.com");

//Generate Crypto-URL valid for 10 minutes
String url = CryptoUrl.toEmit(signal).within(Duration.ofMinutes(10));

```