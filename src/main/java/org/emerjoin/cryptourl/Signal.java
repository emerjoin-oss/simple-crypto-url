package org.emerjoin.cryptourl;


import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Response;
import java.time.Instant;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

/**
 * Represents an event to which there will be a reaction.
 * The event is triggered by a Crypto-URL.
 */
public abstract class Signal {

    protected static final String TYPE_CLAIM_NAME = "type";
    private static ObjectMapper OBJECT_MAPPER;

    private transient Response response;

    static {
        initialize();
    }

    private static void initialize(){
        OBJECT_MAPPER = new ObjectMapper();
        OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
                false);
    }


    public String url(URLValidity validity){
        if(validity==null)
            throw new IllegalArgumentException("validity must not be null");
        CryptoUrlContext context = CryptoUrlContext.get();
        CryptoUrlSettings config = context.settings();

        Algorithm algorithm = Algorithm.HMAC512(config.getSecret());
        JWTCreator.Builder builder = JWT.create();
        if(validity.isHard()){
            Instant instant = Instant.now();
            Instant expiryInstant = instant.plus(validity.duration());
            builder.withExpiresAt(new Date(expiryInstant.
                    toEpochMilli()));
        }

        Map<String,Object> map = this.toMap();
        for(String key: map.keySet()){
            Object value = map.get(key);
            if(value!=null)
                builder.withClaim(key,value.toString());
        }

        String jwt = builder.withClaim(TYPE_CLAIM_NAME,getClass().getSimpleName()).sign(
                algorithm);
        return String.format("%s?%s=%s",config.getBaseURL(),
                config.getKeyParamName(),
                jwt);
    }

    private Map<String,Object> toMap(){
        try {
            return OBJECT_MAPPER.convertValue(this, Map.class);
        }catch (RuntimeException ex){
            throw new TriggerConversionException(this,
                    ex);
        }
    }

    public void setResponse(Response response){
        if(response==null)
            throw new IllegalArgumentException("response must not be null");
        this.response = response;
    }

    Optional<Response> getResponse(){

        return Optional.ofNullable(this.
                response);

    }

    public String toString(){
        return toMap().toString();
    }


    static <T extends Signal> T fromMap(Map<String,String> map, Class<T> type){
        try {
            return OBJECT_MAPPER.convertValue(map, type);
        }catch (RuntimeException ex){
            throw new TriggerConversionException(type,map,
                    ex);
        }
    }

}
