package org.emerjoin.cryptourl;

import java.time.Duration;

public class CryptoUrlBuilder {

    private Signal signal;

    CryptoUrlBuilder(Signal signal){
        this.signal = signal;
    }

    public String within(Duration duration){
        if(duration==null)
            throw new IllegalArgumentException("duration must not be null");
        return this.signal.url(URLValidity.fixed(duration));
    }

    public String anytime(){
        return this.signal.url(URLValidity.dynamic());
    }

}
