package org.emerjoin.cryptourl;

public final class CryptoUrl {

    private static <T extends Signal> CryptoUrlBuilder to(T signal){
        if(signal==null)
            throw new IllegalArgumentException("signal must not be null");
        return new CryptoUrlBuilder(
                signal);
    }

    public static <T extends Signal> CryptoUrlBuilder toIssue(T signal){
        return to(signal);
    }

    public static <T extends Signal> CryptoUrlBuilder toTrigger(T signal){
        return to(signal);
    }

    public static <T extends Signal> CryptoUrlBuilder toEmit(T signal){
        return to(signal);
    }

    public static <T extends Signal> CryptoUrlBuilder toCause(T signal){
        return to(signal);
    }

    public static <T extends Signal> CryptoUrlBuilder toGet(T signal){
        return to(signal);
    }

}
