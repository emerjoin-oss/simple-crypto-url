package org.emerjoin.cryptourl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Path("/callback")
@Consumes(MediaType.TEXT_PLAIN)
@Produces(MediaType.TEXT_HTML)
public class CryptoUrlResource {

    private static final Logger LOGGER = LogManager.getLogger(CryptoUrlResource.class);

    @GET
    public Response process(@QueryParam("key") String key){
        LOGGER.info("Handling Crypto-URL Request...");
        CryptoUrlContext context = CryptoUrlContext.get();
        CryptoUrlSettings config = context.settings();
        if(key==null||key.isEmpty()) {
            LOGGER.info("URL Key missing: "+config.getKeyParamName());
            return Response.status(Response.Status.BAD_REQUEST)
                    .build();
        }
        LOGGER.debug("Callback Key = {}",key);
        LOGGER.info("Validating Key...");
        JWTVerifier verifier = JWT.require(Algorithm.HMAC512(config.getSecret())).build();
        try {
            DecodedJWT jwt = verifier.verify(key);
            Optional<Signal> signalOptional = this.decodeSignal(jwt);
            if(!signalOptional.isPresent()) {
                LOGGER.warn("There is no signal matching the Callback Key");
                return Response.status(Response.Status.NOT_FOUND)
                        .build();
            }
            LOGGER.info("Signal found");
            Signal signal =  signalOptional.get();
            LOGGER.debug("Signal Type: "+signal.getClass().getSimpleName());
            LOGGER.debug("Signal: {}"+signal.toString());
            LOGGER.info("Propelling signal");
            context.propeller().propel(signal);
            LOGGER.info("Signal propelled");

            Optional<Response> responseOptional = signal.getResponse();
            responseOptional.ifPresent(response -> {
                LOGGER.info("Found a Response for signal");
            });
            return responseOptional.orElseGet(() -> {
                LOGGER.info("No Response found for signal. Returning default");
                return Response.ok().build();
            });

        }catch (TokenExpiredException ex) {
            LOGGER.warn("Expired callback token provided: "+ key);
            return Response.status(Response.Status.GONE)
                    .build();
        }catch (JWTVerificationException ex){
            LOGGER.error("Callback verification token provided: "+key);
            return Response.status(Response.Status.BAD_REQUEST)
                    .build();
        }

    }

    private Optional<Signal> decodeSignal(DecodedJWT jwt){
        CryptoUrlContext context = CryptoUrlContext.get();
        String type = jwt.getClaim(Signal.TYPE_CLAIM_NAME).asString();
        Optional<Class<? extends Signal>> optionalClazz = context.
                getTriggerClassByTypeName(type);
        if(!optionalClazz.isPresent()){
            return Optional.empty();
        }
        Map<String,String> claimsMap = new HashMap<>();
        jwt.getClaims().forEach((key,value) -> {
            claimsMap.put(key,value.asString());
        });
        LOGGER.debug("ClaimsMap: "+claimsMap);
        return Optional.of(Signal.fromMap(claimsMap,
                optionalClazz.get()));
    }

}
