package org.emerjoin.cryptourl;

public class CryptoUrlSettings {

    private static final String KEY_PARAM_NAME = "key";
    private String baseURL;
    private String secret;

    public CryptoUrlSettings(String baseUrl, String secret){
        if(baseUrl==null||baseUrl.isEmpty())
            throw new IllegalArgumentException("baseUrl must not be null nor empty");
        if(secret==null)
            throw new IllegalArgumentException("secret must not be null");
        this.baseURL = baseUrl;
        this.secret = secret;
    }

    public String getBaseURL() {
        return baseURL;
    }

    public String getSecret() {
        return secret;
    }

    public String getKeyParamName() {
        return KEY_PARAM_NAME;
    }
}
