package org.emerjoin.cryptourl;



import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class CryptoUrlContext {

    private CryptoUrlSettings settings;
    private StateChangePropeller propeller;
    private Map<String,Class<? extends Signal>> triggers = new HashMap<>();

    private static CryptoUrlContext INSTANCE = null;

    private CryptoUrlContext(){ }

    CryptoUrlSettings settings(){
        return this.settings;
    }

    StateChangePropeller propeller() {
        return propeller;
    }

    Optional<Class<? extends Signal>> getTriggerClassByTypeName(String type){
        return Optional.ofNullable(this.triggers.
                get(type));
    }

    public void set(){
        INSTANCE = this;
    }

    public static CryptoUrlContext get(){
        if(INSTANCE ==null)
            throw new IllegalStateException("no context was set");
        return INSTANCE;
    }

    public static Builder builder(){

        return  new Builder();

    }


    public static final class Builder {

        private CryptoUrlContext context = new CryptoUrlContext();

        private Builder(){

        }

        public Builder settings(CryptoUrlSettings settings){
            if(settings==null)
                throw new IllegalArgumentException("settings must not be null");
            this.context.settings = settings;
            return this;
        }

        public Builder register(Class<? extends Signal> type){
            if(type==null)
                throw new IllegalArgumentException("type must not be null");
            this.context.triggers.put(type.getSimpleName(),type);
            return this;
        }

        public Builder propeller(StateChangePropeller propeller){
            if(propeller==null)
                throw new IllegalArgumentException("propeller must not be null");
            this.context.propeller = propeller;
            return this;
        }


        public CryptoUrlContext build(){
            if(this.context.settings ==null)
                throw new IllegalStateException("must set settings");
            if(this.context.propeller ==null)
                throw new IllegalStateException("must set a carrier");
            if(this.context.triggers.isEmpty())
                throw new IllegalStateException("must register at least one trigger");
            return this.context;
        }

    }


}
