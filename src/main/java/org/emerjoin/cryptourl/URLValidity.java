package org.emerjoin.cryptourl;

import java.time.Duration;

/**
 * Defines for how long a Crypto-URL remains valid.
 */
public class URLValidity {

    private Duration duration;

    private URLValidity(Duration duration){
        this.duration = duration;
    }

    private URLValidity(){

    }

    boolean isSoft(){
        return duration == null;
    }

    boolean isHard(){
        return duration != null;
    }

    Duration duration(){
        return this.duration;
    }

    public static URLValidity dynamic(){
        return new URLValidity();
    }

    public static URLValidity fixed(Duration duration){
        if(duration==null)
            throw new IllegalArgumentException("duration must not be null");
        return new URLValidity(
                duration);
    }

}
