package org.emerjoin.cryptourl;

public class CryptoURLException extends RuntimeException {

    public CryptoURLException(String message){
        super(message);
    }

    public CryptoURLException(String message, Throwable cause){
        super(message,cause);
    }

}
