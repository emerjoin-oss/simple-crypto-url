package org.emerjoin.cryptourl;

import java.util.Map;

public class TriggerConversionException extends CryptoURLException {

    public TriggerConversionException(Class<? extends Signal> type, Map<String,String> map, Throwable cause) {
        super(String.format("error found while creating a %s instance from %s",type.getSimpleName(),map),
                cause);
    }

    public TriggerConversionException(Signal signal, Throwable cause) {
        super(String.format("Unexpected error found while converting %s instance to Map", signal.getClass().getSimpleName()),
                cause);
    }
}
