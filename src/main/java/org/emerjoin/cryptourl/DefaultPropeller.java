package org.emerjoin.cryptourl;


import javax.enterprise.inject.spi.CDI;

public class DefaultPropeller implements StateChangePropeller {

    @Override
    public void propel(Signal signal) {
        if(signal ==null)
            throw new IllegalArgumentException("trigger must not be null");
        CDI.current().getBeanManager().fireEvent(
                signal);
    }

}
