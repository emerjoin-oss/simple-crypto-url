package org.emerjoin.cryptourl;

/**
 * Propels a {@link Signal}.
 */
public interface StateChangePropeller {

    void propel(Signal signal);

}
